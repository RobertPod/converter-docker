FROM ubuntu:18.10

LABEL author="michal@owsiak.org"
LABEL description="miles per galons to liters per km"

# we need some packages to be installed
RUN apt-get update
RUN apt-get install -y unzip
RUN apt-get install -y curl
RUN apt-get install -y git
RUN apt-get install -y build-essential
RUN apt-get install -y gcc
RUN apt-get install -y vim
RUN apt-get install -y execstack
RUN apt-get install -y cmake

# Some variables we need
# You have to pass them via --build-arg while performing build
# of the container
#
# e.g.: 
# --build-arg jdk_location="https://download.java.net/java/GA/jdk11/13/GPL/openjdk-11.0.1_linux-x64_bin.tar.gz" \

ARG jdk_location=you_have_to_specify_jdk_location_via_argument
ENV jdk_location_bash $jdk_location

# Layout of /opt directories
# /opt
#  |-- converter          - Converter code - source, makefile and (finally) binary
#  |-- src                - sources of additional components
#  |-- usr
#  |   `-- local          - place for additional components built from sources (e.g. googletest)
#  `-- jdk                - JDK will be installed here

RUN mkdir -p /opt/jdk
RUN mkdir -p /opt/src
RUN mkdir -p /usr/local

# We can get JDK from the location specified by user
RUN echo "Getting JDK from: $jdk_location_bash"
WORKDIR /opt
RUN curl -O $jdk_location_bash
RUN find . -name "*gz" -exec tar zxf {} -C /opt/jdk \;

RUN git clone https://gitlab.com/mkowsiak/converter.git
RUN git -C converter pull

# Get google tests and install it inside /opt/usr/local
WORKDIR /opt/src
RUN git clone https://github.com/google/googletest.git
WORKDIR /opt/src/googletest
RUN mkdir -p /opt/src/googletest/build
WORKDIR /opt/src/googletest/build
RUN cmake -DCMAKE_INSTALL_PREFIX:PATH=/opt/usr/local ..
RUN make
RUN make install

CMD /opt/run.sh

ADD run.sh /opt/run.sh
RUN chmod +x /opt/run.sh

