# Units converter - Docker based

This Docker based units converter is an overenginered tool for converting miles per gallon to liters per 100km and vice versa.

By overengineering I mean:

> Overengineering (or over-engineering) is the act of designing a product 
> to be more robust or have more features than often necessary for its 
> intended use, or for a process to be unnecessarily complex or inefficient.
>
> -- Wikipedia

or

> over-engineered - unnecessarily complicated
>
> -- Collins English Dictionary

# Project structure

    .
    |-- README.md                              - this README.md file
    |-- Dockerfile                             - Docker file for creating image
    `-- run.sh                                 - wrapper script that runs everything

# Heavilly over-engineered approach

I know that converting liters per 100km to miles per galon is very simple. After all, it's nothing more than

```
L621:
        .quad   4633941839399343020
L026:
        .quad   4598430520537701517
        .globl  _cltm
_cltm:
        pushq   %rbp
        movq    %rsp, %rbp
        movsd   L621(%rip), %xmm1
        movsd   L026(%rip), %xmm2
        movsd   %xmm0, -8(%rbp)
        mulsd   -8(%rbp), %xmm2
        divsd   %xmm2, %xmm1
        movaps  %xmm1, %xmm0
        popq    %rbp
        retq
```

However, I wanted something really over-engineered. Something where you can find most recent tools covering mostly hyped buzzwords. This is why you can find here `Docker`, `googletest`, `JUnit 5`, `Open JDK`, etc.

![](./images/converter-docker.png) 

# Creating units converter

Before you can proceed, you have to create Docker based container.

# Building

    > git clone https://gitlab.com/mkowsiak/converter-docker.git
    > cd converter-docker
    > docker build -t converter \
    --build-arg jdk_location=\
    "https://download.java.net/java/GA/jdk11/13/GPL/openjdk-11.0.1_linux-x64_bin.tar.gz" \
    .

# Running

Running is very simple. You have two modes of execution: `calculation mode` and `POST mode`. Calculation mode performs calculation. You have to specify two variables: `FROM` - determining whether you are passing liters or miles, and `VAL` - with the value you want to convert.

In `POST mode` you have to pass `SELF_TEST=true` as parameter. Container can either run in test mode or calculation mode. You can't mix modes.

    > docker run -i -t -e FROM=[miles|liters] -e VAL=[value] converter

# Examples

    # converting 9.9 liters per 100km to miles per gallon
    > docker run -i -t -e FROM=liters -e VALUE=9.9 converter

    # converting 25 miles per gallon to liters per 100km
    > docker run -i -t -e FROM=miles -e VALUE=25 converter

# Running POST (power-on self-test)

You can run `POST` instead of calculations. This action will trigger execution of `JUnit` based tests of `Java` components and `googletest` based tests of `C++` and `assembler` based components of the whole solution. To run `POST` start the container following way

    > docker run -i -t -e SELF_TEST=true converter

Once started, container will compile and run tests.

```
openjdk 11.0.1 2018-10-16
OpenJDK Runtime Environment 18.9 (build 11.0.1+13)
OpenJDK 64-Bit Server VM 18.9 (build 11.0.1+13, mixed mode)

...
...

.
+-- JUnit Jupiter [OK]
| +-- LitersToMilesTest [OK]
| | '-- - OK, you're all set. That'll be ah, 94 dollars. [OK]
| '-- MilesToLitersTest [OK]
|   '-- - The Answer to the Great Question... Of Life, the Universe and Everything... Is... [OK]
'-- JUnit Vintage [OK]

Test run finished after 106 ms
[         4 containers found      ]
[         0 containers skipped    ]
[         4 containers started    ]
[         0 containers aborted    ]
[         4 containers successful ]
[         0 containers failed     ]
[         2 tests found           ]
[         0 tests skipped         ]
[         2 tests started         ]
[         0 tests aborted         ]
[         2 tests successful      ]
[         0 tests failed          ]

...
...

Running main() from /opt/src/googletest/googletest/src/gtest_main.cc
[==========] Running 2 tests from 2 test suites.
[----------] Global test environment set-up.
[----------] 1 test from LitersToMilesTest
[ RUN      ] LitersToMilesTest.Equal
[       OK ] LitersToMilesTest.Equal (0 ms)
[----------] 1 test from LitersToMilesTest (0 ms total)

[----------] 1 test from LitersToMilesASMTest
[ RUN      ] LitersToMilesASMTest.Equal
[       OK ] LitersToMilesASMTest.Equal (0 ms)
[----------] 1 test from LitersToMilesASMTest (0 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 2 test suites ran. (0 ms total)
[  PASSED  ] 2 tests.
Running main() from /opt/src/googletest/googletest/src/gtest_main.cc
[==========] Running 2 tests from 2 test suites.
[----------] Global test environment set-up.
[----------] 1 test from MilesToLitersTest
[ RUN      ] MilesToLitersTest.Equal
[       OK ] MilesToLitersTest.Equal (0 ms)
[----------] 1 test from MilesToLitersTest (0 ms total)

[----------] 1 test from MilesToLitersASMTest
[ RUN      ] MilesToLitersASMTest.Equal
[       OK ] MilesToLitersASMTest.Equal (0 ms)
[----------] 1 test from MilesToLitersASMTest (1 ms total)

[----------] Global test environment tear-down
[==========] 2 tests from 2 test suites ran. (3 ms total)
[  PASSED  ] 2 tests.
```

# Known limitations

At the moment error handling is not quite efficient.

# Java code used for converting

converter-docker is based on simple `Java` code that is based on singleton patter with factory pattern applied to native code wrappers. Calculations are performed by `C` based code executed via `JNI`.

You can find source code here: <a href="https://gitlab.com/mkowsiak/converter">converter</a>.

