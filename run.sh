#!/bin/bash

ERROR_MISSING_FROM=false
ERROR_INCORRECT_FROM=false
ERROR_MISSING_VALUE=false
ERROR_INCORRECT_VALUE=false
SELF_TEST_VALUE=false

if [ ! -z "$SELF_TEST" ]; then
  if [[ "${SELF_TEST}" != "true" ]]; then
    echo ""
    echo "Self test can handle only value 'true'"
    echo ""
    exit 1
  else
    SELF_TEST_VALUE=true
  fi
fi

if [ ${SELF_TEST_VALUE} == "false" ]; then
  if [ -z "$FROM" ]; then
    ERROR_MISSING_FROM=true
    ERROR=true
  fi
  
  if [[ ${ERROR_MISSING_FROM} == "false" && "${FROM}" != "miles" && "${FROM}" != "liters" ]]; then
    ERROR_INCORRECT_FROM=true
    ERROR=true
  fi
  
  if [ -z "$VALUE" ]; then
    ERROR_MISSING_VALUE=true
    ERROR=true
  fi
  
  if [[ ${ERROR_MISSING_VALUE} == "false" ]] && ! [[ ${VALUE} =~ ^[0-9]*\.?[0-9]*$ ]]; then
    ERROR_INCORRECT_VALUE=true
    ERROR=true
  fi
  
  if [[ $ERROR == true ]]; then
    echo ""
    echo "I have spotted following errors:"
    echo ""
    [[ $ERROR_MISSING_FROM == true ]] && echo "- FROM argument is missing";
    [[ $ERROR_INCORRECT_FROM == true ]] && echo "- FROM argument is incorrect";
    [[ $ERROR_MISSING_VALUE == true ]] && echo "- VALUE argument is missing";
    [[ $ERROR_INCORRECT_VALUE == true ]] && echo "- VALUE argument is incorrect";
    echo ""
    echo "Make sure to run container following way:"
    echo ""
    echo "> docker run -i -t -e FROM=[miles|liters] -e VALUE=[double_value] converter"
    exit 1
  fi
fi
  
JDK_LOCATION=`ls -1 /opt/jdk`
export JAVA_HOME=/opt/jdk/${JDK_LOCATION}
export PATH=${PATH}:${JAVA_HOME}/bin

export JAVA_MEMORY_SETTINGS="-Xms4G -Xmx6G"

cd /opt/converter
git pull &> /dev/null

make clean > /dev/null 2>&1
make > /dev/null 2>&1
execstack -c lib/libLitersToMiles.so
execstack -c lib/libMilesToLiters.so

if [ ${SELF_TEST_VALUE} == "false" ]; then

cat << EOF
      ***************************************************
          https://github.com/mkowsiak/ConverterDocker
      ***************************************************
        Converter of miles per galons to liters per km 
        -----------------------------------------------
        
        The result of conversion from: $FROM
        Where value is: $VALUE
 
        -----------------------------------------------
EOF

  make VALUE=${VALUE} FROM=${FROM} calculate

else
  # make sure we have Java installed
  if [ -e $JAVA_HOME/bin/java ]; then
    $JAVA_HOME/bin/java --version
  fi
  export GOOGLE_TEST=/opt/usr/local
  make junit-test-run
  make google-test-run
fi

